Ext.define('90DaysDiet.model.DietStart', {
    extend : 'Ext.data.Model',
    config : {
        /*identifier : {
            type : 'sequential',
            prefix: 'ID_',
        },*/
        fields : [
            {name: 'id', type: 'int'},
            {name: 'startDate', type: 'date'}
        ],
        proxy : {
            type: 'localstorage',
            id: 'start-diet'
        }
    }
});