Ext.define('90DaysDiet.model.DietDay', {
	extend : 'Ext.data.Model',
	config : {
		identifier: {
			type: 'uuid'
		},
		fields : [
			{name : 'id', type : 'int'},
			{name : 'date', type : 'date', dateFormat: 'c'},
			{name : 'type',  type : 'string'}
		]
	}
});