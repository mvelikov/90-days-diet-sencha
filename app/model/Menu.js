Ext.define('90DaysDiet.model.Menu', {
    extend: 'Ext.data.Model',

    config: {
        fields: [
            {name: 'id', type: 'string'},
            {name: 'text', type: 'string'},
            {name : 'title', type : 'string'}
            /*{name: 'source',      type: 'string'},
            {name: 'animation',   type: 'auto'},
            {name: 'limit',       type: 'auto'},
            {name: 'preventHide', type: 'boolean'},
            {name: 'view',        type: 'string'}*/
        ]
    }
});
