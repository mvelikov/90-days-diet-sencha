Ext.define('90DaysDiet.model.Rule', {
    extend : 'Ext.data.Model',
    config : {
        fields: [
            {name: 'id', type: 'int'},
            {name: 'title', type: 'string'},
            {name: 'description', type: 'string'},
        ]
    }
});