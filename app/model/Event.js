/**
 * TouchCalendar.model.Event
 */
Ext.define('90DaysDiet.model.Event', {

	extend: "Ext.data.Model",

	config: {
		identifier: {
			type: 'uuid'
		},
		fields: [{
				name: 'event',
				type: 'string'
			}, 
			{
				name: 'refId',
				type : 'string'
			},
			{
				name: 'weight',
				type: 'float'
			},  {
				name: 'wasteWeight',
				type: 'float'
			}, {
				name: 'start',
				type: 'date',
				dateFormat: 'c'
			}, {
				name: 'end',
				type: 'date',
				dateFormat: 'c'
			}, {
				name: 'css',
				type: 'string'
			}]
	}
});