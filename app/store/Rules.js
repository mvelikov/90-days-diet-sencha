Ext.define('90DaysDiet.store.Rules', {
    extend : 'Ext.data.Store',
    require : ['90DaysDiet.model.Rule'],
    config : {
        model   : '90DaysDiet.model.Rule',
        autoLoad: true,
        proxy   : {
            type : 'ajax',
            url : 'resources/data/rules.json',
            reader : {
                type : 'json',
                rootProperty : 'rules'
            }
        }
    }
});