Ext.define('90DaysDiet.store.Menu', {
	alias: 'store.Menu',
	extend: 'Ext.data.Store',
	requires: ['90DaysDiet.model.Menu'],

	config : {
		model : '90DaysDiet.model.Menu',
		data : [
    	{
            text: 'Home',
            id: 'intro'
        },
        {
            text: 'Information',
            id: 'info'
        },
        {
            text: 'Rules',
            id: 'rulesContainer'
        },
        {
            text: 'Calendar',
            id: 'calendarWrapper'
        },
        
    ],
		defaultRootProperty : 'items',
	}
});