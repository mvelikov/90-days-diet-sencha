(function () {

    var root = {
        id: 'root',
        text: '90 Days Diet',
        items: [
            {
                text : 'Home',
                view : 'Home',
                leaf : true,
                id : 'home',
                animation : {
                    type : 'slide'
                },
                options : {
                    leftButton : {
                        hidden : true
                    },
                    rightButton : {
                        hidden : true
                    },
                    devInfoButton : {
                        hidden : true
                    }
                }
            },
            {
                text : 'Information',
                view : 'Info',
                leaf : true,
                id : 'info',
                animation : {
                    type : 'slide'
                },
                options : {
                    leftButton : {
                        hidden : true
                    },
                    rightButton : {
                        hidden : true
                    },
                    devInfoButton : {
                        hidden : true
                    }
                }
            },
            {
                text : 'Rules',
                view : 'Rules',
                leaf : true,
                id : 'rules',
                animation : {
                    type : 'slide'
                },
                options : {
                    leftButton : {
                        hidden : true
                    },
                    rightButton : {
                        hidden : true
                    },
                    devInfoButton : {
                        hidden : true
                    }
                }
            },
            {
                text : 'Calendar',
                view : 'TouchCalendar',
                leaf : true,
                id : 'touchCalendar',
                animation : {
                    type : 'slide'
                },
                options : {
                    leftButton : {
                        hidden : false
                    },
                    rightButton : {
                        hidden : false
                    },
                    devInfoButton : {
                        hidden : true
                    }
                }
            }/*,
            {
                text : 'Developer',
                view : 'DeveloperInfo',
                leaf : true,
                id : 'devInfo',
                animation : {
                    type : 'slide'
                },
                options : {
                    leftButton : {
                        hidden : true
                    },
                    rightButton : {
                        hidden : true
                    },
                    devInfoButton : {
                        hidden : true
                    }
                }
            }*/
        ]
    };
/*
    root.items.push(animations, {
        text: 'Touch Events',
        id: 'touchevents',
        view: 'TouchEvents',
        leaf: true
    }, {
        text: 'Data',
        id: 'data',
        items: [
            {
                text: 'Nested Loading',
                view: 'NestedLoading',
                leaf: true,
                id: 'nestedloading'
            },
            {
                text: 'JSONP',
                leaf: true,
                id: 'jsonp'
            },
            {
                text: 'YQL',
                leaf: true,
                id: 'yql'
            },
            {
                text: 'Ajax',
                leaf: true,
                id: 'ajax'
            }
        ]
    }, {
        text: 'Media',
        id: 'media',
        items: [
            {
                text: 'Video',
                leaf: true,
                id: 'video'
            },
            {
                text: 'Audio',
                leaf: true,
                id: 'audio'
            }
        ]
    });
*/

    Ext.define('90DaysDiet.store.Views', {
        alias: 'store.Demos',
        extend: 'Ext.data.TreeStore',
        requires: ['90DaysDiet.model.View'],

        config: {
            model: '90DaysDiet.model.View',
            root: root,
            defaultRootProperty: 'items'
        }
    });
})();
