/**
 * TouchCalendar.store.Events
 */
Ext.define('90DaysDiet.store.Events', {

    extend: 'Ext.data.Store',

    config: {
	    storeId : 'Events',
        model   : '90DaysDiet.model.Event',
	    proxy   : {
		    type    : 'ajax',
		    url     : 'resources/data/eventData.json',
		    reader  : {
			    type        : 'json',
			    rootProperty : 'rows'
		    }
	    },
	    autoLoad: true
    },

    constructor: function(config){
        this.callParent(arguments);
    }

});