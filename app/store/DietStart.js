Ext.define('90DaysDiet.store.DietStart', {
    extend : 'Ext.data.Store',
    require : [
        '90DaysDiet.model.DietStart'
    ],
    config : {
        model : '90DaysDiet.model.DietStart',
        autoLoad : true
    }
});