Ext.define('90DaysDiet.store.Menus', {
	extend: 'Ext.data.Store',
	requires: ['90DaysDiet.model.Menu'],

	config : {
		model : '90DaysDiet.model.Menu',
		data : [
    	{
            id : 'protein-day',
            title : 'Protein Day',
            text : 'During <strong>Protein Day</strong> you are encouraged to have meat and dairy products - cheese, milk, yogurt etc. Eggs are also a good choice for this day.<br />You could combine the main meal with a salad of your choice but keep in mind that too much garnishing could have a bad impact on your diet.'
        },
        {
            id: 'beans-day',
            title : 'Starch Day',
            text : 'Today is your <strong>Starch Day</strong> you could have beans, peas, green beans, lentils, potatoes and some others. You could have them either boiled or baked.<br /> Any vegetable salad is allowed without dressing!'
        },
        {
            id : 'carbohydrate-day',
            title : 'Carbohydrate Day',
            text : 'Today is your <strong>Carbohydrate Day</strong>, you could choose your daily meal out of pasta, bread, sweets, piza, salt cake and make others. Please mind that you should have one type of food per meal.'
        },
        {
            title : 'Fruit Day',
            text : 'During <strong>Fruit Day</strong> only fruits are allowed - one type of fruit is considered to be one meal.',
            id : 'fruit-day'
        },
        {
            id : 'water-day',
            title : 'Water Day',
            text : 'Today is the most difficult of all days - <strong>Water Day</strong>!<br />Only water is allowed during this day.'
        }
        
    ],

	}
});