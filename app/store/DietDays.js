Ext.define('90DaysDiet.store.DietDays', {
	extend : 'Ext.data.Store',
	requires : [
		'Ext.data.proxy.LocalStorage'
	],
	config : {
		storeId : 'DietDays',
		model : '90DaysDiet.model.Event',
		proxy : {
			type : 'localstorage',
			id : 'diet-days'
		},
		autoLoad : true,
	}
});