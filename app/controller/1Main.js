Ext.define('90DaysDiet.controller.Main', {
    extend : 'Ext.app.Controller',

    config : {
        refs : {
            resetButton : 'button[action=resetCalendar]',
            startCalendar : 'button[action=startCalendar]',
            calendar : 'calendarWrapper',
            datePicker : 'datepicker',
            rulesList : 'rulesList',
            devInfoButton : '#devInfoButton',
            devInfo : 'devInfo',
            backToHome : '#backToHome',
            intro : 'intro',
            main : 'main',
            info : '#info',
            nav : '#mainMenu',
        },

        control : {
            picker : {
                //hide : 'getPickedDate',
                pick : 'getPickedDate',
            },
            nav : {
                itemtap: 'onNavTap'
            },
            /*datePicker : {
                itemtap : 'initTest',
            },*/
            resetButton : {
                tap : 'resetCalendar'
            },
            startCalendar : {
                tap : 'startCalendar',
            },
            rulesList : {
                initialize : 'initRulesList'
            },
            calendar : {
//                beforeshow 
                initialize : 'showCalendar',
                show : 'showDatepicker',
                //initialize : 'initCalendar',
            },
            devInfoButton : {
                tap : 'showDevInfo',
            },
            backToHome : {
                tap : 'goToHome',
            },
            main : {
                //activeitemchange : 'changeMainTab',
                show : 'removeMask',
            },
        },
        routes : {
            'about' : 'showDevInfo',
           /* 'home' : 'goToHome',
            'info' : 'goToInfo',
            'calendar' : 'goToCalendar',
            'list' : 'goToList',*/
        },
        stack : [],
    },
    msgBox : null,
    cnt : 0,
    onNavTap : function (self, index, target, record) {
        this.getMain().getItems().each(function (item, index) {
            var it = item.getId();
            if (it != 'mainMenu') {
                item.destroy();
            }
        },
         this);
        var id = record.getId();
        view = Ext.widget(id);

        this.getMain().animateActiveItem(view, { type: 'slide'});
    },
    hideMsgBox : function () {
        var box = Ext.getCmp(this.msgBox);
        if (box && !box.isHidden()) {
            box.hide();
            this.msgBox = null;
        }
    },
    getPickedDate : function (btn, val, slot, opt) {
        if (typeof (val) === 'object') {
            var store = Ext.getStore('DietStart');
            store.add({startDate : val});
            store.sync();
            Ext.getCmp('datepicker').refresh();
        }
    },
    removeMask : function () {
        Ext.Viewport.setMasked(false);
    },
    hideDayOverlay : function () {
        var overlay = Ext.getCmp('dayOverlay');
        if (overlay && !overlay.isHidden())  {
            overlay.hide();
        }
    },
    goToHome : function() {

        if (!this.getMain()) {
            this.main = Ext.widget('main');
        }
        this.hideDayOverlay();
        this.hideMsgBox();
        Ext.Viewport.setActiveItem(this.getMain());
        this.getMain().setActiveItem(0);
        Ext.Viewport.setMasked(false);
    },

    showDevInfo : function () {
        //Ext.Viewport.add(Ext.create('SeparateDining.view.DeveloperInfo'))
        
        if (!this.getDevInfo()) {
            this.devInfo = Ext.widget('devInfo');
        }
        this.hideMsgBox();
        this.hideDayOverlay();
        /*this.getApplication().getHistory().add(new Ext.app.Action({
            url: 'about'
        }), true);*/

        Ext.Viewport.setActiveItem(this.getDevInfo());

    },

    firstVisit : true,

    initRulesList : function () {
        //console.log(Ext.getStore('Rules'));
    },
    showDatepicker : function () {
        if (this.firstVisit) {
            /*var datePicker = Ext.create('Ext.picker.Date', {
                yearFrom : (new Date()).getFullYear() - 1,
                yearTo : (new Date()).getFullYear(),
            });*/
            if (!this.picker) {
                this.picker = Ext.widget('picky');
            }
		    Ext.Viewport.add(this.picker);
		    this.picker.show();return;
            this.startCalendar();
            this.firstVisit = false;
		}
    },
    showCalendar : function () {

        Ext.Viewport.setMasked({xtype : 'loadmask'});
        if (this.firstVisit) {
			//var datePicker = Ext.create('Ext.picker.Date');
			//Ext.Viewport.add(datePicker);
			//datePicker.show();
			//return;
        	var store = Ext.getStore('DietStart');

        	store.load();
        	if (typeof(store.getAt(0)) === 'undefined') {
               this.firstVisit = true;
               Ext.Viewport.setMasked(false);
               return;
         	   store.add({startDate : new Date()});
         	   store.sync();
         	   Ext.getCmp('datepicker').refresh();
        	} else {

        		try {
        			var startDate = store.getAt(0).data.startDate,
                    startDateTS = startDate.getTime();
        			var oneDay = 60 * 60 * 24 * 1000, days90 = 90 * oneDay;
        			var endDateTS = startDateTS + days90; // timestamp
        			var endDate = new Date(endDateTS);

        			var nextStartDateTS = endDateTS + days90; // timestamp
        			var nextStartDate = new Date(nextStartDateTS);
        			var today = new Date(), todayTS = today.getTime();

                    if (startDateTS < todayTS
                        && !startDate.sameDateAs(today)
                        && todayTS < endDateTS
                        && !endDate.sameDateAs(today)) {
                        Ext.getCmp('datepicker').refresh();
                    } else if (endDateTS + oneDay <= todayTS 
                        && !endDate.sameDateAs(today)
                        && todayTS < nextStartDateTS
                        && !nextStartDate.sameDateAs(today)) {
                        Ext.Msg.alert('', 'msg', Ext.emptyFn);
                        var days = (nextStartDateTS - today) / (1000 * 60 * 60 * 24);
                        Ext.Msg.alert('', 'There are ', + Math.ceil(days) + ' more days left to the begining of the next 90 days cycle!', Ext.emptyFn);
                    } else if (nextStartDateTS <= todayTS) {
                        Ext.getStore('DietStart').removeAll(false);
                        store.add({startDate : new Date()});
                        store.sync();
                        Ext.getCmp('datepicker').refresh();
                        Ext.Msg.alert('', 'Your new cycle starts today!', Ext.emptyFn);
                    }
        		} catch (e) {
        			//console.log(e);
    			}
        	}
        }
        if (this.firstVisit === true) {
        	this.firstVisit = false;
        }
        Ext.Viewport.setMasked(false);
    },
    resetCalendar : function () {

        if (typeof(Ext.getStore('DietStart').getAt(0)) !== 'undefined') {
            if (Ext.getStore('DietStart').getAt(0).data.startDate !== null) {
                Ext.Msg.confirm('', 'Are you sure you want to end your diet?', function(res) {
                    if (res === 'yes') {
                        Ext.getStore('DietStart').removeAll(false);
                        Ext.getStore('DietStart').sync();
                        this.firstVisit = true;
                        Ext.getCmp('datepicker').refresh();
                    }
                }, this);
            }
        } else {
            this.msgBox = Ext.Msg.alert('', 'You haven\'t started your diet yet!', Ext.emptyFn).getId();
        }
    },
    startCalendar : function () {
        if ((typeof(Ext.getStore('DietStart').getAt(0)) === 'undefined') 
            || (Ext.getStore('DietStart').getAt(0).data.startDate === null)) {
                Ext.Msg.confirm('', 'Is your diet starting today?', function(res) {
                    if (res === 'yes') {
                        var store = Ext.getStore('DietStart');
                        store.add({startDate : new Date});
                        store.sync();
                        Ext.getCmp('datepicker').refresh();
                    }
                });
        }
    },
});

