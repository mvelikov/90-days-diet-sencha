/**
 * @class 90DaysDiet.controller.Main
 * @extends Ext.app.Controller
 *
 * This is an abstract base class that is extended by both the phone and tablet versions. This controller is
 * never directly instantiated, it just provides a set of common functionality that the phone and tablet
 * subclasses both extend.
 */
Ext.define('90DaysDiet.controller.Main', {
    extend: 'Ext.app.Controller',

    config: {
        /**
         * @private
         */
        viewCache: [],

        refs: {
            nav: '#mainNestedList',
            main: 'mainview',
            toolbar: '#mainNavigationBar',
            /*sourceButton: 'button[action=viewSource]',*/
            leftButton : '#leftButton',
            rightButton : '#rightButton',
            devInfoButton : '#devInfoButton',
            touchCalendarView : 'touchcalendarview',
            /*sourceOverlay: {
                selector: 'sourceoverlay',
                xtype: 'sourceoverlay',
                autoCreate: true
            }*/
        },

        control: {
            /*sourceButton: {
                tap: 'onSourceTap'
            },*/
            nav: {
                itemtap: 'onNavTap'
            },
            devInfoButton : {
                tap : 'onDevInfoTap'
            },
            leftButton : {
                tap : 'onDietStartTap'
            },
            rightButton : {
                tap : 'onDietEndTap'
            },
            touchCalendarView : {
                eventtap : 'onEventTap'
             }
        },

        routes: {
            'view/:id': 'showViewById',
            'menu/:id': 'showMenuById'
        },

        /**
         * @cfg {Ext.data.Model} currentDemo The Demo that is currently loaded. This is set whenever showViewById
         * is called and used by functions like onSourceTap to fetch the source code for the current demo.
         */
        currentDemo: undefined
    },
    overlay : false,

    onEventTap : function (item) {
        var /*eventStore = Ext.getStore('Events'),*/
            daysStore = Ext.getStore('Menus'),
            id = item.get('refId'),
            day = daysStore.getById(id);
            this.overlay = Ext.Viewport.add({
                xtype : 'tooltip',
                html : day.get('text')
            });
            Ext.ComponentQuery.query('#day-toolbar')[0].setTitle(day.get('title'));

            this.overlay.show();
            
    },

    onDietEndTap : function () {
        var store = Ext.getStore('DietDays');
        store.load();
        if (typeof(store.getAt(0)) !== 'undefined') {
            Ext.Msg.confirm('', 'Do you want to end your diet today?', function (res) {
                if (res === 'yes') {
                    store.removeAll(false);
                    store.sync();
                    this.getTouchCalendarView().refresh();
                }
            }, this);
        }
    },

    onDietStartTap : function () {
        var store = Ext.getStore('DietDays');
        store.load();

        if ((typeof(store.getAt(0)) === 'undefined') /*
            || (store.getAt(0).data.startDate === null)*/) {
                Ext.Msg.confirm('', 'Is your diet starting today?', function(res) {
                    if (res === 'yes') {
                        var tmpDate = new Date(),
                            dayStore = Ext.getStore('Menus'),
                            daysData = dayStore.getData(),
                            day;

                        for (var i = 1, y = 0; i <= 90; i++, y++) {

                            if (i % 29 == 0) {
                                day = dayStore.getById('water-day');
                            } else {
                                day = daysData.getAt(y % 4);
                            }

                            tmpDate = Ext.Date.add((new Date()), Ext.Date.DAY, y);
                            var id = day.get('id');
                            store.add({
                                'event' : day.get('title'),
                                'start' : tmpDate,
                                'end' : tmpDate,
                                'css' : id,
                                'refId' : id
                            });
                            store.sync();
                        }
                        this.getTouchCalendarView().refresh();
                        //console.log();
                    }
                }, this);
        }
    },
    onDevInfoTap : function () {

    },

    /**
     * Finds a given view by ID and shows it. End-point of the "demo/:id" route
     */
    showViewById: function (id) {
        var nav = this.getNav(),
            view = nav.getStore().getNodeById(id);

        this.showView(view);
        this.setCurrentDemo(view);
        this.hideSheets();
    },

    /**
     * @private
     * In the kitchen sink we have a large number of dynamic views. If we were to keep all of them rendered
     * we'd risk causing the browser to run out of memory, especially on older devices. If we destroy them as
     * soon as we're done with them, the app can appear sluggish. Instead, we keep a small number of rendered
     * views in a viewCache so that we can easily reuse recently used views while destroying those we haven't
     * used in a while.
     * @param {String} name The full class name of the view to create (e.g. "90DaysDiet.view.Forms")
     * @return {Ext.Component} The component, which may be from the cache
     */
    createView: function (item) {
        var name = this.getViewName(item),
            cache = this.getViewCache(),
            ln = cache.length,
            limit = item.get('limit') || 20,
            view, i = 0, j, oldView;

        for (; i < ln; i++) {
            if (cache[i].viewName === name) {
                return cache[i];
            }
        }

        if (ln >= limit) {
            for (i = 0, j = 0; i < ln; i++) {
                oldView = cache[i];
                if (!oldView.isPainted()) {
                    oldView.destroy();
                } else {
                    cache[j++] = oldView;
                }
            }
            cache.length = j;
        }
        Ext.Viewport.setMasked({xtype : 'loadmask'});
        view = Ext.create(name);
        view.viewName = name;
        cache.push(view);
        this.setViewCache(cache);
        Ext.Viewport.setMasked(false);
        return view;
    },

    /**
     * @private
     * Returns the full class name of the view to construct for a given Demo
     * @param {90DaysDiet.model.Demo} item The demo
     * @return {String} The full class name of the view
     */
    getViewName: function (item) {

        var name = item.get('view') || item.get('text'),
            ns = '90DaysDiet.view.';

        if (name == 'TouchEvents') {
            if (this.getApplication().getCurrentProfile().getName() === 'Tablet') {
                return ns + 'tablet.' + name;
            } else {
                return ns + 'phone.' + name;
            }
        } else {
            return ns + name;
        }
    },

    /**
     * we iterate over all of the floating sheet components and make sure they're hidden when we
     * navigate to a new view. This stops things like Picker overlays staying visible when you hit
     * the browser's back button
     */
    hideSheets: function () {
        Ext.each(Ext.ComponentQuery.query('sheet'), function (sheet) {
            sheet.setHidden(true);
        });
    }
});
