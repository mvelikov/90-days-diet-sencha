Ext.define('90DaysDiet.profile.Phone', {
    extend: '90DaysDiet.profile.Base',

    config: {
        controllers: ['Main'],
        views: ['Main']
    },

    isActive: function() {
        return Ext.os.is.Phone || Ext.Viewport.getWindowWidth() <= 640;
    },

    launch: function() {
        Ext.create('90DaysDiet.view.phone.Main');

        this.callParent();
    }
});
