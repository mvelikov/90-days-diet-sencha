Ext.define('90DaysDiet.view.Tooltip', {
    extend : 'Ext.ux.Tooltip',
    requires : [
        'Ext.ux.Tooltip'
    ],
    xtype        : 'tooltip',
    config : {
        width: Ext.os.deviceType == 'Phone' ? 260 : 400,
        height: Ext.os.deviceType == 'Phone' ? '70%' : 400,
        items : [{
            docked: 'top',
            xtype: 'toolbar',
            id : 'day-toolbar',
            title: 'Tooltip',
            items : [
                {
                    align : 'left',
                    xtype : 'button',
                    html : '<div class="close-button">x</div>',
                    ui : 'round',
                }]
            }]
    },
    onTap : function (e) {
        this.hide();
    }
});