Ext.define('90DaysDiet.view.phone.Main', {
    extend: 'Ext.dataview.NestedList',
    requires: ['Ext.TitleBar'],

    id: 'mainNestedList',

    config: {
        fullscreen: true,
        title: '90DaysDiet',
        useTitleAsBackText: false,
        layout: {
            animation: {
                duration: 250,
                easing: 'ease-in-out'
            }
        },

        store: 'Views',

        toolbar: {
            id: 'mainNavigationBar',
            xtype : 'titlebar',
            docked: 'top',
            title : 'How to Make 90DaysDiet',

            items: [
                {
                    xtype : 'button',
                    id: 'leftButton',
                    hidden: true,
                    align : 'right',
                    ui    : 'action',
                    action: 'startDiet',
                    text  : 'Start'
                },
                {xtype : 'spacer'},
                {
                    xtype : 'button',
                    id: 'rightButton',
                    hidden: true,
                    align : 'right',
                    ui    : 'action',
                    action: 'endDiet',
                    text  : 'Clear'
                },
            ]
        }
    }
});
