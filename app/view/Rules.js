Ext.define('90DaysDiet.view.Rules', {
    extend : 'Ext.Panel',
    require : [
        '90DaysDiet.store.Rules',
        '90DaysDiet.view.RulesList'
    ],
    //xtype : 'rulesContainer',
    xtype : 'rulesList',
    config : {
        title : 'Rules',
        iconCls : 'list',
        autoDestroy : false,
        layout : 'fit',
        //margin : Ext.os.deviceType == 'Phone' ? '0' : '0 100 0 100',
        items : [
            {
                xtype: 'list',
                store: 'Rules',
                itemTpl: '<div class="rule"><h3 class="title">{title}</h3><div class="description">{description}</div></div>',
                //variableHeights: false,
                styleHtmlContent : true
            }
        ]
    }
});