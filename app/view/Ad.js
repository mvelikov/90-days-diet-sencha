Ext.define('90DaysDiet.view.Ad', {
    extend : 'Ext.Panel',
    xtype: 'ad',

    config : {
        id : 'ad',
        scrollable : false,
        //maxWidth : '99%',
        //margin : 0,
        //padding : 0,
        style : {
            '-webkit-box-shadow' : '0px -2px 5px #888',
            'box-shadow' : '0px -2px 5px #888',
            'border-top' : '1px solid #a2a2a2',
        },
        html : '<a id="ad-link" style="color: #145f98; border-top : 1px solid #e2e2e2" href="http://grabo.bg/?affid=9319&email=" target="_blank"><b>Ексклузивни промоции в твоя град от Grabo.bg, с отстъпки от 50% до 90%!</b></a>',
        //html : '<iframe id="ad-frame" src="http://b.grabo.bg/?city=&affid=9319&size=468x60&output=iframe" width="100%" height="58" style="width:100%; height:60px; border:0px solid; overflow:hidden;" border="0" frameborder="0" scrolling="no"></iframe>',
        //html : '<iframe id="ad-frame" src="ad.html" width="100%" height="50px" frameBorder="0" scrolling="no"></iframe>'
    },
    
    
});
