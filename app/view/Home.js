Ext.define('90DaysDiet.view.Home', {
    extend : 'Ext.Container',
    xtype : 'intro',
    alias : 'widget.home',
    
    config : {
        layout : {
            type : 'vbox',
            align : 'middle'
        },
        scrollable : true,
        title : 'Home',
        iconCls : 'home',
        styleHtmlContent : true,
        //padding : Ext.os.deviceType == 'Phone' ? '0' : '0 15% 0 15%',
        //marginRight : Ext.os.deviceType == 'Phone' ? 0 : 100,
        /*items : [
            {
                xtype : 'titlebar',
                docked : 'top',
                items : [
                    {
                        xtype : 'button',
                        iconCls : 'user',
                        align : 'right',
                        iconMask: true,
                        action : 'showDevInfo',
                        id : 'devInfoButton',
                    },
                    {
                        xtype: 'image',
                        width:48,
                        height:32,
                        src: Ext.os.deviceType == 'Phone' ? 'resources/icons/Icon~ipad@2x.png' : '',
                    }
                ]
            }
        ],*/
        html : ['<h1>90 Days Diet</h1>',
        '<h3>Improve your health and appearance</h3>',
        '<p>This application is built to help you follow the simple rules of 90 Days Diet. Using the information, rules and calendar in the application you will easily fulfil you dream to get helthier and improve you look.</p>',
        ].join(''),
    }
});
