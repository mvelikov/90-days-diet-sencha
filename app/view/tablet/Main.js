Ext.define('90DaysDiet.view.tablet.Main', {
    extend: 'Ext.Container',
    xtype: 'mainview',

    requires: [
        'Ext.dataview.NestedList',
        'Ext.navigation.Bar'
    ],

    config: {
        fullscreen: true,

        layout: {
            type: 'card',
            animation: {
                type: 'slide',
                direction: 'left',
                duration: 250
            }
        },

        items: [
            {
                xtype : 'home'
            },
            {
                id: 'mainNestedList',
                xtype : 'nestedlist',
                useTitleAsBackText: false,
                docked: 'left',
                width : 250,
                //styleHtmlContent : true,
                store: 'Views'
                /*, getItemTextTpl: function(node) {
                    return '<img src="resources/images/{id}/icon.png" class="menu-icon" /><span class="menu-title" style="">{text}</span>';
                    
                }*/
            },
            {
                id: 'mainNavigationBar',
                xtype : 'titlebar',
                docked: 'top',
                ui : 'light',
                title : '90 Days Diet',
                items: [{
                    xtype : 'button',
                    id: 'rightButton',
                    hidden: true,
                    align : 'right',
                    ui    : 'action',
                    action: 'endDiet',
                    text  : 'Clear'
                },
                {
                    xtype : 'button',
                    id: 'leftButton',
                    hidden: true,
                    align : 'left',
                    ui    : 'action',
                    action: 'startDiet',
                    text  : 'Start'
                },
                {
                    xtype : 'button',
                    id : 'devInfoButton',
                    hidden : true,
                    align : 'right',
                    iconCls : 'user',
                    iconMask : true,
                    action : 'showDevInfo',
                }]
            }
        ]
    }
});
