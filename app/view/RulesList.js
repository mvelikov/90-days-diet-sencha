Ext.define('90DaysDiet.view.RulesList', {
    extend : 'Ext.List',
    
    require : [
        '90DaysDiet.store.Rules'
    ],
    alias : 'widget.rulesList',
    config : {
        title : 'Rules',
        iconCls : 'list',
        store : 'Rules',
        
        styleHtmlContent : true,
        disableSelection : true,
        itemTpl : [
            '<div class="rule"><h3 class="title">{title}</h3><div class="description">{description}</div></div>'
        ],
        items: [
           /* {
                xtype : 'titlebar',
                title : 'Rules',
                docked : 'top',
                style : Ext.os.deviceType != 'Phone' ? 'display : none' : '',
            },*/
        ]
    }
});
