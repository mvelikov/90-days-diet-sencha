Ext.define('90DaysDiet.view.DeveloperInfo', {
    extend : 'Ext.Container',
    xtype : 'devInfo',
    config : {
        fullscreen : true,
        layout : {
            type : 'vbox',
            align : 'middle'
        },

        scrollable : true,
        title : 'Developer',
        items : [],
        //centered : true,
        html : 'Developed by: <h2 style="text-align: left">Mihail Boyanov Velikov</h2>\
        <h3>Web developer, mobile developer, cloud developer</h3>\
        <p>Email: <a href="mailto:90days@mihailvelikov.eu">90days@mihailvelikov.eu</a></p>\
        <p>Web site: <a href="http://mihailvelikov.eu" target="_blank">http://mihailvelikov.eu</a></p>',
        hideAnimation : {
            type: 'flip',
            duration : 250,
            easing : 'ease-out'
        },
        scrollable : true,
        styleHtmlContent : true,
    },
    
    
});