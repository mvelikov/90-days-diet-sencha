Ext.define('90DaysDiet.view.Info', {
    extend : 'Ext.Panel',
    xtype : 'info',
    
    requires : [
    'Ext.carousel.Carousel',
    ],
    config : {
        //fullscreen : true,
        scrollable : false,
        layout : {
            type : 'vbox',
            align : 'stretch'
        },
        title : 'Info',
        iconCls : 'info',

        items : [
/*        {
            xtype : 'titlebar',
            title : 'Info',
            docked : 'top'
        },*/
        {
            xtype : 'carousel',
            flex : 1,
            //margin : Ext.os.deviceType == 'Phone' ? '0' : '0 100 0 100',
            defaults : {
                styleHtmlContent : true,
                scrollable : {
                    direction : 'vertical',
                    directionLock : true
                }
            },
            items : [
         
            {
                html: ['<h3 class="page-title">Purpose</h3>',
                '<p>The purpose of 90 Days Diet is to change body metabolic proceses not just losing some pounds but keeping the weight you\'ve gained. In order to achieve complete results you must not end your diet premature.</p>',
                ].join(''),
                cls : 'card intro'
            },
            {
                html: [
                    '<h3 class="page-title">Instructions</h3>',
                    '<p>Whole diet cycle lasts for 90 days and you may lose up to 60 pounds, which is very specific to your body. You could have your usual amount of food for a meal but keep in mind that best results are achieved with small meal portions. Diet itself is not so tough excluding the "Water day", the most important part is to follow the days order.</p>',
                ].join(''),
                cls : 'card purpose'
            },
            {
                html: '<h3 class="page-title">Additional Instructions</h3><p>If you don\'t lose weight in the first 10 days even though your efforts don\'t give up and don\'t lose faith. Keep with your schedule because it is possible that you lose a dosen of pounds during your "Water days".</p><p>Diet cycle goes on for 90 days exactly and after first 90 days you have to rest for another 90 days in order to recover. If feel fine after this period you can start the cycle again.</p>',
                cls : 'card precautions'
            },
            {
                html: '<h3 class="page-title">Meal Cycles</h3>\
        <p>Body is a machine which requires strict meals sequence. Researches show human metabolism has three main cycles:</p><ul><li class="li-style text-align-left">04:00 to 12:00 - cleansing cycle</li><li class="li-style text-align-left">12:00 to 20:00 - meals cycle</li><li class="li-style text-align-left">20:00 to 04:00 - digesting cycle</li></ul>',
                cls: 'card cycles'
            },
            {
                html: '<h3 class="page-title">Cleansing Cycle</h3>\
        <h4>04:00 to 12:00</h4><p>By this time you have to consume as little food as possible although some fruits are allowed. A lot of water is required, some tea or diet drinks. Essential for the 90 Days Diet is the everyday breakfast with fruits only!</p>',
                cls: 'card cleansing'
            },
            {
                html: '<h3 class="page-title">Meal Cycles</h3>\
        <p>Body is a machine which requires strict meals sequence. Researches show human metabolism has three main cycles:</p><ul><li class="li-style text-align-left">04:00 to 12:00 - cleansing cycle</li><li class="li-style text-align-left">12:00 to 20:00 - meals cycle</li><li class="li-style text-align-left">20:00 to 04:00 - digesting cycle</li></ul>',
                cls: 'card eating'
            },
            {
                html: '<h3 class="page-title">Digesting Cycle</h3>\
        <h4>20:00 to 04:00</h4><p>During this cycle food is digested and absorbed by the body that\'s why you shouldn\'t eat. If you can\'t stand it in the begining have a fruit, glass of water or sugarless tea.</p>',
                cls: 'card digesting'
            },
            {
                html: '<h3 class="page-title">Order</h3>\
        <p>The most essential part of the diet is the day order. Following the right days order as well as the right combination of food will bring best results - pounds and inches.</p><p>The diet is based on 4 days regular meals and one "Water Day" each 29th day. It always begins with "Protein Day" continues with "Starch Day" then "Carbohydrate Day" and ends with a "Fruit Day".</p>',
                cls: 'card day-order'
            },
            {
                html: '<h3 class="page-title">Protein Day</h3>\
        <p>Proteins are big molecules built of aminoacids, these are animal products at most. Eight of the most important aminoacids are not produced by human body and that\'s why we have to get them from the consumed food. Proteins are necessary for the growth because our body is built by them - hear, nails, muscles, etc. They are important for many other metabolic proceses in human body.</p>',
                cls: 'card protein-day'
            },
            {
                html: '<h3 class="page-title">Starch Day</h3>\
        <p>Products: rise, white beans, peas, corn, lentils, potatoes.</p><p>All of the above are rich in proteins, starch, B vitamins, potassium, phosphorus, magnesium but containes a lot cellulose that irritates the stomach and produce gas. Most starchy foods are suitable for diabetics.</p><p><u>Important:</u>&nbsp;Cooking starchy foods you may use salt and other tasty spices which are necessary for the metabolism.</p>',
                cls: 'card beans-day'
            },
            {
                html: '<h3 class="page-title">Carbohydrate Day</h3>\
        <p>Products: sugar, pastry, pasta, pizza, sweets, etc.</p><p>Carbohydrates produce energy for the body, improve brain activity and stabilize the central nervous system. When you consume more carbohydrates that the body could transform into glucose they are stacked in fats. They could be divided to monocarbohydrates and polycarbohydrates by their structure.</p><p>The main reason for fattening is overconsuming carbohydrates that\'s why you have to combine your meals wisely during these days.</p>',
                cls: 'card carbohydrate-day'
            },
            {
                html: '<h3 class="page-title">Fruit Day</h3>\
        <p>Fruits contain lots of vitamins and energy thus during these days you will feel extremely well in addition you will be protected against some illnesses. Sooner or later "Fruit Day" will become your favourite one and you will be eagerly waiting for it. Essential for this day is not to mix different types of fruit in one meal: breakfast - one type of fruit, lunch - another, dinner - third one.</p>',
                cls: 'card fruit-day'
            },
            {
                html: '<h3 class="page-title">Water Day</h3>\
        <p>This day follows the fruit day and repeats once every 29 days, there are exactly 3 such days</p><p>"Water Day" is the most difficult one it\'s positive effect will make you feel better for at least two weeks. After a "Water Day" you may lose a couple of pounds and will cleanse your body from toxins.</p>',
                cls: 'card '
            },
            ]
        
        }
        ]
    
    }   
    
});
