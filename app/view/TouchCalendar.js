/**
 * TouchCalendar.view.EventListPanel
 */
Ext.define('90DaysDiet.view.TouchCalendar', {

    extend: 'Ext.Panel',

	requires: [
		'Ext.dataview.List',
		'Ext.layout.Fit',
		'Ext.ux.TouchCalendarMonthEvents',
		'Ext.ux.TouchCalendarView',
		'Ext.ux.TouchCalendarEvents'
	],

    alias: 'widget.TouchCalendar',

    config: {
	    title   : 'Events List',
	    layout  : 'fit',
	    iconCls : 'user'
    },

    initialize: function(){
	    this.callParent(arguments);

	    this.setItems([{
		    xclass      : 'Ext.ux.TouchCalendarView',
		    itemId      : 'calendarView',
		    minDate     : Ext.Date.add((new Date()), Ext.Date.MONTH, -1),
		    maxDate     : Ext.Date.add((new Date()), Ext.Date.DAY, 180),
		    viewMode    : 'month',
		    weekStart   : 0,
		    value       : new Date(),
		    eventStore  : Ext.getStore('DietDays'),

		    plugins     : [Ext.create('Ext.ux.TouchCalendarEvents', {
			    eventBarTpl: '{event}'
		    })]
	    }/*, {
		    docked      : 'bottom',
		    xtype       : 'list',
		    height      : 110,
		    itemTpl     : '{event} {location}',
		    store       : Ext.create('Ext.data.Store', {
			    model: '90DaysDiet.model.Event',
			    data: []
		    })
	    }*/]);


    }

});