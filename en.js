var lng = {
	home : {
		title : 'Home',
		content : '<h1>90 Days Diet</h1>\
		<h3>Improve your health and appearance</h3>\
		<p>This application is built to help you follow the simple rules of 90 Days Diet. Using the information, rules and calendar in the application you will easily fulfil you dream to get helthier and improve you look.</p>'
	},
	info : {
		title : 'Info',
		purpose : '<h3 class="page-title">Purpose</h3>\
		<p>The purpose of 90 Days Diet is to change body metabolic proceses not just losing some pounds but keeping the weight you\'ve gained. In order to achieve complete results you must not end your diet premature.</p>',
		instructions : '<h3 class="page-title">Instructions</h3>\
		<p>Whole diet cycle lasts for 90 days and you may lose up to 60 pounds, which is very specific to your body. You could have your usual amount of food for a meal but keep in mind that best results are achieved with small meal portions. Diet itself is not so tough excluding the "Water day", the most important part is to follow the days order.</p>',
		additional_instructions : '<h3 class="page-title">Additional Instructions</h3><p>If you don\'t lose weight in the first 10 days even though your efforts don\'t give up and don\'t lose faith. Keep with your schedule because it is possible that you lose a dosen of pounds during your "Water days".</p><p>Diet cycle goes on for 90 days exactly and after first 90 days you have to rest for another 90 days in order to recover. If feel fine after this period you can start the cycle again.</p>',
		meal_cycles : '<h3 class="page-title">Meal Cycles</h3>\
		<p>Body is a machine which requires strict meals sequence. Researches show human metabolism has three main cycles:</p><ul><li class="li-style text-align-left">04:00 to 12:00 - cleansing cycle</li><li class="li-style text-align-left">12:00 to 20:00 - meals cycle</li><li class="li-style text-align-left">20:00 to 04:00 - digesting cycle</li></ul>',
		cleansing_cycle : '<h3 class="page-title">Cleansing Cycle</h3>\
		<h4>04:00 to 12:00</h4><p>By this time you have to consume as little food as possible although some fruits are allowed. A lot of water is required, some tea or diet drinks. Essential for the 90 Days Diet is the everyday breakfast with fruits only!</p>',
		meal_cycle: '<h3 class="page-title">Meals Cycle</h3>\
		<h4>12:00 to 20:00</h4><p>There are no meals restrictions during this cycle. The amount of food is not essential but the timing between two meals is what matters. It\'s compulsory that you stick with the days order.</p><p>During "Protein Day" it is best to have about 4 hours between two meals, during "Starch Day" and "Carbohydrate Day" it\'s advisable to leave at least 2 hours.</p><p>During "Fruit Day" it\'s necessary to consume more fruits and you have to keep in mind that one type of fruit is considered one meal. 1 or 2 hours between two meals is required.</p>',
		digesting_cycle : '<h3 class="page-title">Digesting Cycle</h3>\
		<h4>20:00 to 04:00</h4><p>During this cycle food is digested and absorbed by the body that\'s why you shouldn\'t eat. If you can\'t stand it in the begining have a fruit, glass of water or sugarless tea.</p>',
		order : '<h3 class="page-title">Order</h3>\
		<p>The most essential part of the diet is the day order. Following the right days order as well as the right combination of food will bring best results - pounds and inches.</p><p>The diet is based on 4 days regular meals and one "Water Day" each 29th day. It always begins with "Protein Day" continues with "Starch Day" then "Carbohydrate Day" and ends with a "Fruit Day".</p>',
		protein_day : '<h3 class="page-title">Protein Day</h3>\
		<p>Proteins are big molecules built of aminoacids, these are animal products at most. Eight of the most important aminoacids are not produced by human body and that\'s why we have to get them from the consumed food. Proteins are necessary for the growth because our body is built by them - hear, nails, muscles, etc. They are important for many other metabolic proceses in human body.</p>',
		pulse_day : '<h3 class="page-title">Starch Day</h3>\
		<p>Products: rise, white beans, peas, corn, lentils, potatoes.</p><p>All of the above are rich in proteins, starch, B vitamins, potassium, phosphorus, magnesium but containes a lot cellulose that irritates the stomach and produce gas. Most starchy foods are suitable for diabetics.</p><p><u>Important:</u>&nbsp;Cooking starchy foods you may use salt and other tasty spices which are necessary for the metabolism.</p>',
		carbohydrate_day : '<h3 class="page-title">Carbohydrate Day</h3>\
		<p>Products: sugar, pastry, pasta, pizza, sweets, etc.</p><p>Carbohydrates produce energy for the body, improve brain activity and stabilize the central nervous system. When you consume more carbohydrates that the body could transform into glucose they are stacked in fats. They could be divided to monocarbohydrates and polycarbohydrates by their structure.</p><p>The main reason for fattening is overconsuming carbohydrates that\'s why you have to combine your meals wisely during these days.</p>',
		fruit_day : '<h3 class="page-title">Fruit Day</h3>\
		<p>Fruits contain lots of vitamins and energy thus during these days you will feel extremely well in addition you will be protected against some illnesses. Sooner or later "Fruit Day" will become your favourite one and you will be eagerly waiting for it. Essential for this day is not to mix different types of fruit in one meal: breakfast - one type of fruit, lunch - another, dinner - third one.</p>',
		water_day : '<h3 class="page-title">Water Day</h3>\
		<p>This day follows the fruit day and repeats once every 29 days, there are exactly 3 such days</p><p>"Water Day" is the most difficult one it\'s positive effect will make you feel better for at least two weeks. After a "Water Day" you may lose a couple of pounds and will cleanse your body from toxins.</p>'
	},
	calendar : {
		title : 'Calendar',
		clean : 'Clean up',
		messages : {
	        'protein-day' : {
	            title : 'Protein',
	            text : 'Today is your <strong>Protein Day</strong>, please mind your daily meals!'
	        },
	        'fruit-day' : {
	            title : 'Fruit',
	            text : 'Today is your <strong>Fruit Day</strong>, emphasize on current seazon fruits!',
	        },
	        'carbohydrate-day' : {
	            title : 'Carbohydrate',
	            text : 'Today is your <strong>Carbohydrate Day</strong>, treat yourself!'
	        },
	        'beans-day' : {
	            title : 'Starch',
	            text : 'Today is your <strong>Starch Day</strong>, find something suitable to eat!'
	        },
	        'water-day' : {
	            title : 'Water',
	            text : 'Today is the most difficult of all days - <strong>Water Day</strong>!'
	        },
	    },
	},
	dev_info : {
		title : 'Developer',
		content : 'Developed by: <h2 style="text-align: left">Mihail Boyanov Velikov</h2>\
		<h3>Web developer, mobile developer, cloud developer</h3>\
		<p>Email: <a href="mailto:separate.dining@mihailvelikov.eu">separate.dining@mihailvelikov.eu</a></p>\
        <p>Web site: <a href="http://mihailvelikov.eu" target="_blank">http://mihailvelikov.eu</a></p>'
	},
	rules : {
		title : 'Rules',
	},
	confirm_diet_start_today : 'Is your diet starting today?',
	new_cycle_starts : 'Your new cycle starts today!',
	there_are : 'There are ',
	days_left : ' more days left to the begining of the next 90 days cycle!',
	diet_not_started : 'You haven\'t started your diet yet!',
	end_diet : 'Are you sure you want to end your diet?',
	back : 'Back',
	days : ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
	months : ['January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'],
    rules_list : [
    {id: "0",description: "You have to obay the day order - protein day, starch day, carbohydrate day and fruit day.","title": "Day Order Rule"},
    {id: "1",description: "Before you start with the diet check your blood pressure and do it regularly - at least once a month.","title": "Blood Pressure Rule"},
    {id: "2",description: "Cook your meals using olive oil instead of sunflower oil and spices but do not overuse them.","title": "Oil and Spices Rule"},
    {id: "3",description: "This diet is perfect for people suffering from high blood pressure, if you have a pills prescription you have to discuss this with your doctor.","title": "Pills Rule"},
    {id: "4",description: "You have to obay meals hours, if you starve before your meal have a fruit or a glass of fruit juice.","title": "Meals Hours Rule"},
    {id: "5",description: "Having a fresh is considered as one meal, so be careful.","title": "Fresh Rule"},
    {id: "6",description: "Drinking soda is absolutely unexceptable during diet cycles.","title": "Soda Rule"},
    {id: "7",description: "Alcohol is not allowed during diet cycles.","title": "Alcohol Rule"},
    {id: "8",description: "Best results are achieved if you start drining your tea and coffee wihout sugar.","title": "Sugar Rule"},
    {id: "9",description: "It is important to increase time outdoor walking or making exercises.","title": "Exercises Rule"},
    {id: "10",description: "Drinking milk between meals is considered one separate meal, be careful.","title": "Milk Rule"},
    {id: "11",description: "You should be careful about the amount of salt you use with your meals.","title": "Salt Rule"},
    {id: "12",description: "Drinking water or tea while having meal is not adviseable, because it makes food digesture more difficult. You could have a cup of tea or a glass of water half an hour after meal thus you guarantee good food digesture.","title": "Food Digesture Rule"}
]
};
